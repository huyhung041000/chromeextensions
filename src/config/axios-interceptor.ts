import { SERVER_API_URL } from "@/common/constants/constants";
import axios from "axios";

const TIMEOUT = 1000000;
const onRequestSuccess = (config: any) => {
  const token =
    localStorage.getItem("ds-authenticationToken") ||
    sessionStorage.getItem("ds-authenticationToken");
  if (token) {
    if (!config.headers) {
      config.headers = {};
    }
    config.headers.Authorization = `Bearer ${token}`;
  }
  config.timeout = TIMEOUT;
  config.url = `${SERVER_API_URL}${config.url}`;
  return config;
};
const setupAxiosInterceptors = (onUnauthenticated: any) => {
  const onResponseError = (err: any) => {
    const status = err.status || err.response.status;
    if (status === 403 || status === 401) {
      onUnauthenticated();
    }
    return Promise.reject(err);
  };
  if (axios.interceptors) {
    axios.interceptors.request.use(onRequestSuccess);
    axios.interceptors.response.use((res) => res, onResponseError);
  }
};

export { onRequestSuccess, setupAxiosInterceptors };
