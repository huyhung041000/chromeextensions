import { EXCLUDE_API_LOADER } from "@/common/constants/ds-constants";
import axios, { AxiosRequestConfig } from "axios";
import { Store } from "vuex";

let numOfRequest = 0;
let appStore: Store<any>;

const onCallRequest = (config: any) => {
  const url = getUriDeepSignalApi(config);
  if (checkApiLoader(url)) {
    numOfRequest++;
    if (appStore) {
      appStore.commit("SET_LOADING", true);
    }
  }
  return config;
};

function checkApiLoader(url: any) {
  if (!url) {
    return;
  }
  return (
    EXCLUDE_API_LOADER.findIndex((item) =>
      url.toLowerCase().includes(item.toLocaleLowerCase())
    ) === -1
  );
}

function getUriDeepSignalApi(config: AxiosRequestConfig) {
  if (!config) {
    return;
  }
  return axios.getUri(config);
}

function hideLoader(store: Store<any>) {
  if (numOfRequest === 0) {
    // all request is done
    store.commit("SET_LOADING", false);
  }
}

const axiosRequestLoader = (store: Store<any>) => {
  appStore = store;
  const onResponseError = (err: any) => {
    const url = getUriDeepSignalApi(err.config);
    if (checkApiLoader(url)) {
      numOfRequest--;
      hideLoader(store);
    }
    return Promise.reject(err);
  };
  const onResponseSuccess = (res: any) => {
    const url = getUriDeepSignalApi(res.config);
    if (checkApiLoader(url)) {
      numOfRequest--;
      hideLoader(store);
    }
    return Promise.resolve(res);
  };
  if (axios.interceptors) {
    axios.interceptors.request.use(onCallRequest);
    axios.interceptors.response.use(onResponseSuccess, onResponseError);
  }
};

export { axiosRequestLoader };
