import { messages } from "./../assets/i18n";
import { Vuelidate } from "vuelidate";
import VueI18n, { DateTimeFormats } from "vue-i18n";
import { setupAxiosInterceptors } from "./axios-interceptor";
import DsFormatter from "./formatter";

const dateTimeFormats: DateTimeFormats = {
  en: {
    short: {
      year: "numeric",
      month: "short",
      day: "numeric",
      hour: "numeric",
      minute: "numeric",
    },
    medium: {
      year: "numeric",
      month: "short",
      day: "numeric",
      weekday: "short",
      hour: "numeric",
      minute: "numeric",
    },
    long: {
      year: "numeric",
      month: "long",
      day: "numeric",
      weekday: "long",
      hour: "numeric",
      minute: "numeric",
    },
  },
  ko: {
    short: {
      year: "numeric",
      month: "short",
      day: "numeric",
      hour: "numeric",
      minute: "numeric",
    },
    medium: {
      year: "numeric",
      month: "short",
      day: "numeric",
      weekday: "short",
      hour: "numeric",
      minute: "numeric",
    },
    long: {
      year: "numeric",
      month: "long",
      day: "numeric",
      weekday: "long",
      hour: "numeric",
      minute: "numeric",
    },
  },
};

export function initI18N(vue: any) {
  vue.use(VueI18n);
  return new VueI18n({
    dateTimeFormats,
    silentTranslationWarn: true,
    formatter: new DsFormatter(),
    locale: "en", // set locale
    fallbackLocale: "en", // set fallback locale
    messages, // set locale messages
  });
}

export function initVueApp(vue: any) {
  vue.use(Vuelidate);
  setupAxiosInterceptors(() => {});
}
