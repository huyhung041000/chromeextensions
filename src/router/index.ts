import LoginViewVue from "@/views/LoginView.vue";
import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "login",
    component: LoginViewVue,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
