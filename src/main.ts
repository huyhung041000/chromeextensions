import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import LoginService from "./service/login.service";
import PhoneService from "./service/phone.service";
import PublicService from "./service/public.service";
import RegisterService from "./service/register.service";
import store from "./store";
import * as config from "./config/config";
import Multiselect from "vue-multiselect";

Vue.config.productionTip = false;
Vue.component("multiselect", Multiselect);
const i18n = config.initI18N(Vue);
config.initVueApp(Vue);
new Vue({
  router,
  store,
  provide: {
    publicService: () => new PublicService(),
    registerService: () => new RegisterService(),
    phoneService: () => new PhoneService(),
    loginService: () => new LoginService(),
  },
  i18n,
  render: (h) => h(App),
}).$mount("#app");
