import { appendParamToRequest } from "@/common/utils/ds-util";
import axios from "axios";
import $ from "jquery";

export default class PublicService {
  public getLocationByIp(ipAddress: string) {
    return axios.get("api/public/getLocationByIp/" + ipAddress);
  }
  previewInfoByUrl(url: string, connectomeId?: string) {
    return axios.get(
      "api/public/previewByUrl?url="
        .concat(url)
        .concat("&connectomeId=")
        .concat(connectomeId)
    );
  }
  getIpFromClient() {
    return new Promise((resolve) => {
      $.getJSON("https://api.ipify.org/?format=json", (data) => {
        resolve(data.ip);
      });
    });
  }
  public getCountryCode() {
    return axios.get("api/public/getCountryCode");
  }
}
