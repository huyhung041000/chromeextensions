export const messages = {
  en: {
    register: {
      phone: {
        selectCountries: "Enter your country code.",
        phoneNumber: "Enter your phone number.",
        sendCode: "Request verification.",
        confirmCode: "Enter your verification code.",
        name: "Enter your name.",
        phone: "Phone Number",
        email: "Email",
      },
    },
    login: {
      title: "Login",
      signin: "Sign In",
      form: {
        password: "Password",
        "password.placeholder": "Your password",
        rememberme: "Keep me logged in.",
        button: "Login",
        phonenumber: "Phone number",
        send: "Send",
        code: "Code",
        "code.placeholder": "Enter code",
        confirm: "Confirm",
        ainame: "Your AI Name",
        "ainame.placeholder": "Select Connectome",
      },
      messages: {
        error: {
          authentication:
            "<strong>Failed!</strong> Please check the login credential and try again",
          phone: "The phone number entered invalid.",
          "phone.maxlength": "This field should be less than 50 characters.",
          code: "The verification you entered is incorrect.",
          "code.maxlength": "This field should be less than 10 characters.",
          lockedAccount:
            "Your account has been locked. Please Try again in 5 minutes",
        },
      },
      password: {
        forgot: "Forgot password?",
      },
      join: "Join Deepsignal",
      notjoin: "Do you want to register an account?",
      signUp: "Sign Up",
      changeLanguage: "Change Language",
    },
  },
  ko: {
    register: {
      phone: {
        selectCountries: "국가 번호 입력",
        phoneNumber: "없이 번호 입력",
        sendCode: "인증 요청",
        confirmCode: "인증번호 입력",
        name: "이름 입력",
        phone: "전화 번호",
        email: "이메일",
      },
    },
    login: {
      title: "인증",
      signin: "로그인",
      form: {
        password: "비밀번호",
        "password.placeholder": "당신의 비밀번호",
        rememberme: "자동 로그인",
        button: "로그인",
        phonenumber: "전화번호",
        send: "보내다",
        code: "암호",
        "code.placeholder": "코드를 입력",
        confirm: "확인하다",
        ainame: "AI 이름",
        "ainame.placeholder": "커넥톰 선택",
      },
      messages: {
        error: {
          authentication:
            "<strong>인증 실패!</strong> 로그인 정보를 확인하고 다시 시도해 주세요.",
          phone: "입력하신 전화번호가 정확하지 않습니다.",
          "phone.maxlength": "이 필드는 50자를 초과할 수 없습니다.",
          code: "입력한 코드가 올바르지 않습니다.",
          "code.maxlength": "이 필드는 10자를 초과할 수 없습니다.",
        },
      },
      password: {
        forgot: "비밀번호를 잊으셨나요?",
      },
      join: "DeepSignal 회원가입",
      notjoin: "아직 가입하지 않았나요?",
      signUp: "회원가입",
      changeLanguage: "언어 선택",
    },
  },
};
