import axios from "axios";
import Component from "vue-class-component";
import { Vue, Inject } from "vue-property-decorator";
import PublicService from "@/service/public.service";
import dsPhoneValidAuthVue from "@/views/ds-phone-valid-auth.vue";
import dsEmailValidAuthVue from "@/views/ds-email-valid-auth.vue";

@Component({
  components: {
    "ds-phone-valid-auth": dsPhoneValidAuthVue,
    "ds-email-valid-auth": dsEmailValidAuthVue,
  },
})
export default class LoginComponent extends Vue {
  @Inject("publicService")
  publicService: () => PublicService;

  loginFailedAttempt = 0;
  langKey = "en";
  public authenticationError = null;
  public jwt = null;
  public rememberMe = false;
  public connectomes = [];
  public connectomeSelected = "";
  public isValid = false;
  public isShowConnectome = false;
  selectedLogin = true;
  // Test phone number: 5571981265131
  public formModel = {
    countryCode: "",
    phoneNumber: "",
    type: 1,
    code: "",
    email: "",
    emailCode: "",
    login: "",
    timeZone: "",
  };

  public isLoginQR = true;

  swiperOptions = {
    effect: "fade",
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
      renderBullet(index, className) {
        return '<span class="' + className + '">0' + (index + 1) + "</span>";
      },
    },
  };

  public changeTypeLogin() {
    this.isLoginQR = !this.isLoginQR;
  }

  confirm() {
    this.isValid = false;
    if (this.selectedLogin) {
      // @ts-ignore
      this.$refs.dsPhoneComponent.confirm();
    } else {
      // @ts-ignore
      this.$refs.dsEmailComponent.confirm();
    }
  }

  public doLogin(loginName, code) {
    const language = this.$i18n.locale;
    const data = {
      username: loginName,
      password: code,
      rememberMe: this.rememberMe,
      language,
    };
    axios
      .post("api/authenticate", data)
      .then((result) => {
        const bearerToken = result.headers.authorization;
        if (bearerToken && bearerToken.slice(0, 7) === "Bearer ") {
          this.jwt = bearerToken.slice(7, bearerToken.length);
          this.isShowConnectome = false;
          if (this.jwt != null) {
            if (this.rememberMe) {
              localStorage.setItem("ds-authenticationToken", this.jwt);
              sessionStorage.removeItem("ds-authenticationToken");
            } else {
              sessionStorage.setItem("ds-authenticationToken", this.jwt);
              localStorage.removeItem("ds-authenticationToken");
            }
          }
          this.processAfterLoginSuccess(loginName);
        }
        this.authenticationError = false;
        this.publicService()
          .getIpFromClient()
          .then((ip) => {
            axios.put(
              "api/admin/users/updateLastLogin?login=" + loginName + "&ip=" + ip
            );
          });
      })
      .catch(() => {
        this.authenticationError = true;
        this.isValid = true;
      });
  }

  public processAfterLoginSuccess(login: string) {
    this.$router.push("/register");
  }

  public callback(loginName, code) {
    this.doLogin(loginName, code);
  }

  public isActiveLanguage(key: string): boolean {
    return key === this.$store.getters.currentLanguage;
  }

  public logout(): void {}

  routeToRegister(navigate, e) {
    navigate(e);
  }

  mounted() {
    document.body.setAttribute("data-menu", "login");
  }

  handleTypeLogin(type) {
    this.selectedLogin = type;
  }
}
